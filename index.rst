Inicio
======

.. toctree::
   :maxdepth: 2
   :caption: Documentación para usuarios

   userguide
   faq
   clients

.. toctree::
   :maxdepth: 2
   :caption: Documentación para administradores

   sysadmins

.. toctree::
   :maxdepth: 2
   :caption: Documentación para desarrolladores

   developers


Por ahora, la mayor parte de la Documentación se encuentra en `el viejo wiki <https://github.com/e14n/pump.io/wiki>`_.

Check out the `community information <https://github.com/e14n/pump.io/wiki/Community>`_.

