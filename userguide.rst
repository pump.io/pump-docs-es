Guía básica de usuario
======================

Pump.io is a decentralized/federated social network, composed by many servers, where users can choose where to sign up for an account, and communicate with Pump users from any other public Pump.io server.

It's currently under construction, but there are many things that can be done already. You can follow people, post notes, images and other media, and like, share or reply to other people's posts.

If you don't have a Pump account yet, `click here for some help <http://polari.us/dokuwiki/doku.php?id=gettingstartedwithpumpio#getting_an_account_on_pumpio>`_.

When you `register <http://polari.us/dokuwiki/doku.php?id=gettingstartedwithpumpio#getting_an_account_on_pumpio>`_, you'll be in front of the default web interface, but that's just the door. You can choose to interact with the Pump network using this web interface, or you can install a desktop client (or *app*), a mobile client, or interact from 3rd party web services of different kinds. Here's a `comprehensive list of clients and services <clients.html>`_.

One of the first things you'll want to do is start following some people. Here's `some info about that <https://github.com/e14n/pump.io/wiki/Who-to-follow,-most-shared,-most-liked...-%28informal-stats%29>`_. Check out `this list of users by language <https://github.com/e14n/pump.io/wiki/Users-by-language>`_, where you can also add yourself.

But before that, it would be really useful to fill in some data in your profile, and uploading an avatar.
Saying something about yourself in the Bio helps people find you, and makes it more likely that people with similar interests will follow you, so it makes starting interacting with people easier and faster.

It also helps to post a public "hello world" message, so people who find you have something to comment on.

If you used identi.ca in the past, take a look at `this note about some of the big changes that identi.ca has undergone <https://identi.ca/jankusanagi/note/IpfiW7kBQYuXt6H7odMzQQ>`_, now that it's part of the Pump.io network.

***

These are some other guides explaining how Pump.io works, how to get started, and providing usage tips by users:

* `Getting Started With Pump.io <http://polari.us/dokuwiki/doku.php?id=gettingstartedwithpumpio>`_ by `Stephen Sekula <https://hub.polari.us/steve>`_
* `Who to follow, most shared, most liked... (informal stats) <https://github.com/e14n/pump.io/wiki/Who-to-follow,-most-shared,-most-liked...-%28informal-stats%29>`_
* `Some tips for a better experience using the Pump.io network <https://communicationfreedom.wordpress.com/2014/03/17/pump-io-tips/>`_ by `JanKusanagi <https://identi.ca/jankusanagi>`_
* `FAQ <faq.html>`_ - Frequently Asked Questions

